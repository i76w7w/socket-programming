#include<stdio.h>
#include<winsock2.h>
#include"sockets.h"
void init_winsock(){
	WSADATA wsa_data;
	if (WSAStartup(MAKEWORD(2, 0), &wsa_data) != 0){
		printf("failed to initialize winsock.\n");
		exit(-1);
	}
}
int Socket(int family, int type, int protocol)
{
	int n;
	if ((n = socket(family, type, protocol)) < 0){
		printf("failed to creat a socket.\n");
		exit (- 1);
	}
	return n;
}
void Sockaddr(struct sockaddr_in *server_addr, int family, unsigned short port, unsigned long ip)
{
	server_addr->sin_family = family;
	server_addr->sin_port = htons(port);
	server_addr->sin_addr.s_addr = ip;
}
int Connect(SOCKET sock, struct sockaddr_in *server_addr, int size)
{
	int flag;
	if ((flag = connect(sock, (struct sockaddr *)server_addr, size)) < 0){
		printf("failed to connect.\n");
		return -1;
	}
	return flag;
}
int Send(SOCKET socket, char *buf, int len, int flags)
{
	int result = send(socket, buf, len, flags);
	if (result == SOCKET_ERROR){
		printf("failed to send.\n");
		return -1;
	}
	return result;
}
int Recv(SOCKET socket, char *buf, int len, int flags)
{
	int result = recv(socket, buf, len, flags);
	if (result == SOCKET_ERROR){
		printf("failed to receive.\n");
		return -1;
	}
	return result;
}