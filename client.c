#include<stdio.h>
#include<winsock2.h>
#include"sockets.h"
#pragma comment (lib,"ws2_32.lib")

char http_payload_format[]="GET %s HTTP/1.1\r\n"
"Accept: image/gif, image/jpeg, */*\r\nAccept-Language: zh-cn\r\n"
"Accept-Encoding: gzip, deflate\r\nHost: %s:%d\r\n"
"User-Agent: covert <0.1>\r\nConnection: Keep-Alive\r\n\r\n";

void parse_arguments(argument *arg, int argc, char **argv);

int main(int argc, char ** argv)
{
	if (argc < 2){
		printf("Usage:client.exe server_address\nExample:client.exe http://www.baidu.com\n");
		return 0;
	}
	init_winsock();

	argument arg;
	parse_arguments(&arg,argc,argv);

	SOCKET sock;
	sock = Socket(AF_INET, SOCK_STREAM, 0);

	struct sockaddr_in server_addr;
	Sockaddr(&server_addr, AF_INET, PORT, arg.ip);

	Connect(sock,&server_addr,sizeof(server_addr));

	struct in_addr i_a;
	i_a.S_un.S_addr=arg.ip;
	char buf[HTTP_PAYLOAD_SIZE];
	int buf_len = sprintf(buf, http_payload_format, argv[1], inet_ntoa(i_a), arg.port);
	Send(sock, buf, buf_len, 0);

	for(;Recv(sock, buf, HTTP_PAYLOAD_SIZE, 0)>0;)
		printf("%s\n",buf);
	return 0;
}
void parse_arguments(argument *arg, int argc, char **argv)
{
	char *server=argv[1]+7;
	int i ;
	for (i = 7; argv[1][i]; i++){
		if (argv[1][i] == '/')
			break;
	}
	argv[1][i] = 0;
	unsigned long a = inet_addr(server);
	struct hostent *host_ent;
	if (a == INADDR_NONE){
		host_ent = gethostbyname(server);
		if (host_ent == NULL){
			printf("invalid server[%d]\n",WSAGetLastError());
			exit(-1);
		}
		memcpy(&a, host_ent->h_addr_list[0], host_ent->h_length);
	}
	arg->ip = a;
	sprintf(arg->file_name, argv[1]+i+1);
	arg->port = 80;
	argv[1][i] = '/';
}