#define PORT 80
#define HTTP_PAYLOAD_SIZE 1024
void init_winsock();
int Socket(int family, int type, int protocol);
void Sockaddr(struct sockaddr_in *server_addr,int family,unsigned short port,unsigned long ip);
int Connect(SOCKET sock, struct sockaddr_in *server_addr, int size);
int Send(SOCKET socket,char *buf,int len,int flags);
int Recv(SOCKET socket, char *buf, int len, int flags);
typedef struct
{
	unsigned long ip;
	int port;
	char file_name[HTTP_PAYLOAD_SIZE];
}argument;